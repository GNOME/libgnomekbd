# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Victor Ibragimov <victor.ibragimov@gmail.com>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: Tajik Gnome\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=libgnomekbd&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2013-12-19 14:38+0000\n"
"PO-Revision-Date: 2014-01-28 22:32+0500\n"
"Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>\n"
"Language-Team: \n"
"Language: tg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.6.3\n"

#: ../libgnomekbd/gkbd-indicator.c:422 ../libgnomekbd/gkbd-status.c:560
msgid "XKB initialization error"
msgstr "Хатои омодасозии XKB"

#: ../libgnomekbd/gkbd-keyboard-drawing.c:2485
#, c-format
msgid ""
"Keyboard layout \"%s\"\n"
"Copyright &#169; X.Org Foundation and XKeyboardConfig contributors\n"
"For licensing see package metadata"
msgstr ""
"Тарҳбандии клавиатура \"%s\"\n"
"Ҳуқуқи муаллиф &#169; Ташкилоти X.Org ва саҳмгузорони XKeyboardConfig\n"
"Барои иҷозатдиҳӣ ба бастаи метаиттилоот муроҷиат намоед"

#: ../libgnomekbd/gkbd-keyboard-drawing.c:2635
msgid "Unknown"
msgstr "Номаълум"

#: ../libgnomekbd/gkbd-keyboard-config.c:712
#, c-format
msgid "layout \"%s\""
msgid_plural "layouts \"%s\""
msgstr[0] "тарҳбандии \"%s\""
msgstr[1] "тарҳбандии \"%s\""

#: ../libgnomekbd/gkbd-keyboard-config.c:730
#, c-format
msgid "option \"%s\""
msgid_plural "options \"%s\""
msgstr[0] "имкони \"%s\""
msgstr[1] "имкони \"%s\""

#: ../libgnomekbd/gkbd-keyboard-config.c:738
#, c-format
msgid "model \"%s\", %s and %s"
msgstr "намунаи \"%s\", %s ва %s"

#: ../libgnomekbd/gkbd-keyboard-config.c:739
msgid "no layout"
msgstr "тарҳбандӣ нест"

#: ../libgnomekbd/gkbd-keyboard-config.c:740
msgid "no options"
msgstr "имконот нест"

#: ../libgnomekbd/gkbd-status.c:299
#, c-format
msgid "There was an error loading an image: %s"
msgstr "Ҳангоми боркунии тасвир хатогӣ ба вуҷуд омад: %s"

#: ../libgnomekbd/gkbd-keyboard-display.desktop.in.in.h:1
#: ../libgnomekbd/show-layout.ui.h:1
msgid "Keyboard Layout"
msgstr "Тарҳбандии клавиатура"

#: ../libgnomekbd/gkbd-keyboard-display.desktop.in.in.h:2
msgid "Preview keyboard layouts"
msgstr "Пешнамоиши тарҳбандии клавиатура"

#: ../libgnomekbd/org.gnome.libgnomekbd.desktop.gschema.xml.in.in.h:1
msgid "Default group, assigned on window creation"
msgstr "Гурӯҳи пешфарзе, ки ҳангоми эҷоди равзана таъйин шуд"

#: ../libgnomekbd/org.gnome.libgnomekbd.desktop.gschema.xml.in.in.h:2
msgid "Keep and manage separate group per window"
msgstr "Нигоҳ доштан ва идора кардани гурӯҳи алоҳида барои ҳар равзана"

#: ../libgnomekbd/org.gnome.libgnomekbd.desktop.gschema.xml.in.in.h:3
msgid "Save/restore indicators together with layout groups"
msgstr "Захира/барқарор кардани индикаторҳо бо гурӯҳҳои тарҳбандӣ"

#: ../libgnomekbd/org.gnome.libgnomekbd.desktop.gschema.xml.in.in.h:4
msgid "Show layout names instead of group names"
msgstr "Намоиш додани номҳои тарҳбандӣ ба ҷои номҳои гурӯҳӣ"

#: ../libgnomekbd/org.gnome.libgnomekbd.desktop.gschema.xml.in.in.h:5
msgid ""
"Show layout names instead of group names (only for versions of XFree "
"supporting multiple layouts)"
msgstr ""
"Намоиш додани номҳои тарҳбандӣ ба ҷои номҳои гурӯҳӣ (танҳо барои версияҳои "
"XFree бо дастгирии якчанд тарҳбандӣ)"

#: ../libgnomekbd/org.gnome.libgnomekbd.desktop.gschema.xml.in.in.h:6
msgid "Load extra configuration items"
msgstr "Бор кардани объектҳои танзимотии иловагӣ"

#: ../libgnomekbd/org.gnome.libgnomekbd.desktop.gschema.xml.in.in.h:7
msgid "Load exotic, rarely used layouts and options"
msgstr "Боргирии тарҳбандиҳои ғайриоддӣ, камчин ва имконот"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:1
msgid "The Keyboard Preview, X offset"
msgstr "Пешнамоиши клавиатура, тағйирёбии X"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:2
msgid "The Keyboard Preview, Y offset"
msgstr "Пешнамоиши клавиатура, тағйирёбии Y"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:3
msgid "The Keyboard Preview, width"
msgstr "Пешнамоиши клавиатура, бар"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:4
msgid "The Keyboard Preview, height"
msgstr "Пешнамоиши клавиатура, баландӣ"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:5
msgid "Secondary groups"
msgstr "Гурӯҳҳои иловагӣ"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:6
msgid "Show flags in the applet"
msgstr "Намоиш додани байрақчаҳо дар зербарнома"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:7
msgid "Show flags in the applet to indicate the current layout"
msgstr "Намоиш додани байрақчаҳо дар зербарнома барои намоиши тарҳбандии ҷорӣ"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:8
msgid "The font family"
msgstr "Гурӯҳи шрифт"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:9
msgid "The font family for the layout indicator"
msgstr "Гурӯҳи шрифтҳо барои индикатори тарҳбандӣ"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:10
msgid "The font size"
msgstr "Андозаи шрифт"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:11
msgid "The font size for the layout indicator"
msgstr "Андозаи шрифт барои индикатори тарҳбандӣ"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:12
msgid "The foreground color"
msgstr "Ранги пешзамина"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:13
msgid "The foreground color for the layout indicator"
msgstr "Ранги пешзамина барои индикатори тарҳбандӣ"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:14
msgid "The background color"
msgstr "Ранги пасзамина"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:15
msgid "The background color for the layout indicator"
msgstr "Ранги пасзамина барои индикатори тарҳбандӣ"

#: ../libgnomekbd/org.gnome.libgnomekbd.keyboard.gschema.xml.in.in.h:1
msgid "Keyboard model"
msgstr "Намунаи клавиатура"

#: ../libgnomekbd/org.gnome.libgnomekbd.keyboard.gschema.xml.in.in.h:2
msgid "keyboard model"
msgstr "намунаи клавиатура"

#: ../libgnomekbd/org.gnome.libgnomekbd.keyboard.gschema.xml.in.in.h:3
msgid "Keyboard layout"
msgstr "Тарҳбандии клавиатура"

#: ../libgnomekbd/org.gnome.libgnomekbd.keyboard.gschema.xml.in.in.h:4
msgid "keyboard layout"
msgstr "Тарҳбандии клавиатура"

#: ../libgnomekbd/org.gnome.libgnomekbd.keyboard.gschema.xml.in.in.h:5
msgid "Keyboard options"
msgstr "Имконоти клавиатура"

#: ../test/gkbd-indicator-test.c:66
msgid "Indicator:"
msgstr "Индикатор:"
