# translation of libgnomekbd.master.kn.po to Kannada
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Shankar Prasad <svenkate@redhat.com>, 2009.
# Shankar Prasad <svenkate@redhat.com>, 2009, 2013.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-08-06 14:46+0530\n"
"PO-Revision-Date: 2013-10-27 02:30-0400\n"
"Last-Translator: Shankar Prasad <svenkate@redhat.com>\n"
"Language-Team: Kannada <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: kn\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Zanata 3.2.3\n"

#: ../libgnomekbd/gkbd-indicator.c:422
#: ../libgnomekbd/gkbd-status.c:560
msgid "XKB initialization error"
msgstr "XKB ಆರಂಭಿಸುವಲ್ಲಿ ದೋಷ"

#: ../libgnomekbd/gkbd-keyboard-drawing.c:2485
#, c-format
msgid ""
"Keyboard layout \"%s\"\n"
"Copyright &#169; X.Org Foundation and XKeyboardConfig contributors\n"
"For licensing see package metadata"
msgstr ""
"ಕೀಲಿ ಮಣೆ ವಿನ್ಯಾಸ \"%s\"\n"
"ಹಕ್ಕು &#169; X.Org ಫೌಂಡೇಶನ್ ಹಾಗು XKeyboardConfig ಸಹಾಯಕರು\n"
"ಲೈಸೆನ್ಸಿಂಗ್ ಬಗೆಗಿನ ಮಾಹಿತಿಗಾಗಿ ಪ್ಯಾಕೇಜಿನ ಮೆಟಾಡಾಟವನ್ನು ನೋಡಿ"

#: ../libgnomekbd/gkbd-keyboard-drawing.c:2635
msgid "Unknown"
msgstr "ಗೊತ್ತಿರದ"

#: ../libgnomekbd/gkbd-keyboard-config.c:712
#, c-format
msgid "layout \"%s\""
msgid_plural "layouts \"%s\""
msgstr[0] "ವಿನ್ಯಾಸ \"%s\""
msgstr[1] "ವಿನ್ಯಾಸಗಳು \"%s\""

#: ../libgnomekbd/gkbd-keyboard-config.c:730
#, c-format
msgid "option \"%s\""
msgid_plural "options \"%s\""
msgstr[0] "ಆಯ್ಕೆ \"%s\""
msgstr[1] "ಆಯ್ಕೆಗಳು \"%s\""

#: ../libgnomekbd/gkbd-keyboard-config.c:738
#, c-format
msgid "model \"%s\", %s and %s"
msgstr "ಮಾದರಿ \"%s\", %s ಹಾಗು %s"

#: ../libgnomekbd/gkbd-keyboard-config.c:739
msgid "no layout"
msgstr "ಯಾವುದೆ ವಿನ್ಯಾಸವಿಲ್ಲ"

#: ../libgnomekbd/gkbd-keyboard-config.c:740
msgid "no options"
msgstr "ಯಾವುದೆ ಆಯ್ಕೆ ಇಲ್ಲ"

#: ../libgnomekbd/gkbd-status.c:299
#, c-format
msgid "There was an error loading an image: %s"
msgstr "ಚಿತ್ರವನ್ನು ಲೋಡ್ ಮಾಡುವಲ್ಲಿ ದೋಷ ಉಂಟಾಗಿದೆ: %s"

#: ../libgnomekbd/gkbd-keyboard-display.desktop.in.in.h:1
#: ../libgnomekbd/show-layout.ui.h:1
msgid "Keyboard Layout"
msgstr "ಕೀಲಿಮಣೆ ವಿನ್ಯಾಸ"

#: ../libgnomekbd/gkbd-keyboard-display.desktop.in.in.h:2
msgid "Preview keyboard layouts"
msgstr "ಕೀಲಿಮಣೆ ವಿನ್ಯಾಸಗಳ ಅವಲೋಕನ"

#: ../libgnomekbd/org.gnome.libgnomekbd.desktop.gschema.xml.in.in.h:1
msgid "Default group, assigned on window creation"
msgstr "ಪೂರ್ವನಿಯೋಜಿತ ಗುಂಪು, ವಿಂಡೋ ನಿರ್ಮಾಣದಲ್ಲಿ ನಿಯೋಜಿಸಲಾಗಿದೆ"

#: ../libgnomekbd/org.gnome.libgnomekbd.desktop.gschema.xml.in.in.h:2
msgid "Keep and manage separate group per window"
msgstr "ಪ್ರತಿ ವಿಂಡೋಗಾಗಿ ಪ್ರತ್ಯೇಕ ಗುಂಪನ್ನು ಇರಿಸಿಕೊಳ್ಳಿ ಹಾಗು ನಿರ್ವಹಿಸಿ"

#: ../libgnomekbd/org.gnome.libgnomekbd.desktop.gschema.xml.in.in.h:3
msgid "Save/restore indicators together with layout groups"
msgstr "ವಿನ್ಯಾಸ ಗುಂಪುಗಳೊಂದಿಗೆ ಸೂಚಕಗಳನ್ನು ಉಳಿಸು/ಮರಳಿ ಸ್ಥಾಪಿಸಿ"

#: ../libgnomekbd/org.gnome.libgnomekbd.desktop.gschema.xml.in.in.h:4
msgid "Show layout names instead of group names"
msgstr "ಗುಂಪಿನ ಹೆಸರುಗಳ ಬದಲು ವಿನ್ಯಾಸದ ಹೆಸರುಗಳನ್ನು ತೋರಿಸು"

#: ../libgnomekbd/org.gnome.libgnomekbd.desktop.gschema.xml.in.in.h:5
msgid ""
"Show layout names instead of group names (only for versions of XFree "
"supporting multiple layouts)"
msgstr ""
"ಗುಂಪಿನ ಹೆಸರುಗಳ ಬದಲು ವಿನ್ಯಾಸದ ಹೆಸರುಗಳನ್ನು ತೋರಿಸು (ಕೇವಲ XFree ಬೆಂಬಲವಿರುವ ಅನೇಕ "
"ವಿನ್ಯಾಸಗಳಿಗಾಗಿನ ಆವೃತ್ತಿಗಾಗಿ ಮಾತ್ರ)"

#: ../libgnomekbd/org.gnome.libgnomekbd.desktop.gschema.xml.in.in.h:6
msgid "Load extra configuration items"
msgstr "ಹೆಚ್ಚಿನ ಸಂರಚನಾ ಅಂಶಗಳನ್ನು ಲೋಡ್ ಮಾಡಿ"

#: ../libgnomekbd/org.gnome.libgnomekbd.desktop.gschema.xml.in.in.h:7
msgid "Load exotic, rarely used layouts and options"
msgstr "ನವೀನ, ವಿರಳವಾಗಿ ಬಳಸಲಾದ ಲೇಔಟ್‌ಗಳನ್ನು ಹಾಗು ಆಯ್ಕೆಗಳನ್ನು ಲೋಡ್ ಮಾಡು"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:1
msgid "The Keyboard Preview, X offset"
msgstr "ಕೀಲಿಮಣೆ ಮುನ್ನೋಟ, X ಆಫ್‌ಸೆಟ್"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:2
msgid "The Keyboard Preview, Y offset"
msgstr "ಕೀಲಿಮಣೆ ಮುನ್ನೋಟ, Y ಆಫ್‌ಸೆಟ್"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:3
msgid "The Keyboard Preview, width"
msgstr "ಕೀಲಿಮಣೆ ಮುನ್ನೋಟ, ಅಗಲ"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:4
msgid "The Keyboard Preview, height"
msgstr "ಕೀಲಿಮಣೆ ಮುನ್ನೋಟ, ಎತ್ತರ"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:5
msgid "Secondary groups"
msgstr "ಅಪ್ರಮುಖವಾದ ಗುಂಪುಗಳು"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:6
msgid "Show flags in the applet"
msgstr "ಆಪ್ಲೆಟ್‌ನಲ್ಲಿ ಗುರುತನ್ನು ತೋರಿಸು"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:7
msgid "Show flags in the applet to indicate the current layout"
msgstr "ಪ್ರಸಕ್ತ ವಿನ್ಯಾಸವನ್ನು ಸೂಚಿಸಲು ಆಪ್ಲೆಟ್‌ನಲ್ಲಿ ಗುರುತನ್ನು ತೋರಿಸು"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:8
msgid "The font family"
msgstr "ಅಕ್ಷರಶೈಲಿ ಪರಿವಾರ"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:9
msgid "The font family for the layout indicator"
msgstr "ವಿನ್ಯಾಸ ಸೂಚಕಕ್ಕಾಗಿನ ಅಕ್ಷರಶೈಲಿ ಪರಿವಾರ"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:10
msgid "The font size"
msgstr "ಅಕ್ಷರಶೈಲಿ ಗಾತ್ರ"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:11
msgid "The font size for the layout indicator"
msgstr "ವಿನ್ಯಾಸ ಸೂಚಕಕ್ಕಾಗಿನ ಅಕ್ಷರಶೈಲಿಯ ಗಾತ್ರ"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:12
msgid "The foreground color"
msgstr "ಮುನ್ನೆಲೆ ಬಣ್ಣ"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:13
msgid "The foreground color for the layout indicator"
msgstr "ವಿನ್ಯಾಸ ಸೂಚಕಕ್ಕಾಗಿನ ಮುನ್ನೆಲೆ ಬಣ್ಣ"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:14
msgid "The background color"
msgstr "ಹಿನ್ನೆಲೆ ಬಣ್ಣ"

#: ../libgnomekbd/org.gnome.libgnomekbd.gschema.xml.in.in.h:15
msgid "The background color for the layout indicator"
msgstr "ವಿನ್ಯಾಸ ಸೂಚಕಕ್ಕಾಗಿನ ಹಿನ್ನೆಲೆ ಬಣ್ಣ"

#: ../libgnomekbd/org.gnome.libgnomekbd.keyboard.gschema.xml.in.in.h:1
msgid "Keyboard model"
msgstr "ಕೀಲಮಣೆ ಮಾದರಿ"

#: ../libgnomekbd/org.gnome.libgnomekbd.keyboard.gschema.xml.in.in.h:2
msgid "keyboard model"
msgstr "ಕೀಲಮಣೆ ಮಾದರಿ"

#: ../libgnomekbd/org.gnome.libgnomekbd.keyboard.gschema.xml.in.in.h:3
msgid "Keyboard layout"
msgstr "ಕೀಲಮಣೆ ವಿನ್ಯಾಸ"

#: ../libgnomekbd/org.gnome.libgnomekbd.keyboard.gschema.xml.in.in.h:4
msgid "keyboard layout"
msgstr "ಕೀಲಮಣೆ ವಿನ್ಯಾಸ"

#: ../libgnomekbd/org.gnome.libgnomekbd.keyboard.gschema.xml.in.in.h:5
msgid "Keyboard options"
msgstr "ಕೀಲಮಣೆ ಆಯ್ಕೆಗಳು"

#: ../test/gkbd-indicator-test.c:66
msgid "Indicator:"
msgstr "ಸೂಚಕ:"
